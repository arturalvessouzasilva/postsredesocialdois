"""postredesocial_projeto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from django.conf.urls import url, include, re_path, static


from django.conf import settings


from perfil.views import *
from servico.views import *

urlpatterns = [
    path('admin/', admin.site.urls),

    url(r'^login/', Login.as_view()),
    url(r'^logout/', Logout.as_view()),

    # MODIFICAR
    # A
    # VIEW
    url(r'^resetar_senha/', ReseteSenha.as_view()),
    path('registro/', Registro.as_view()),
    url(r'^updateregistro/$', UpdateRegistro.as_view()),



    # path('documentos/', Documentos.as_view()),


    url(r'^documentos/(?P<categoria_id>[\d]+)/$', Documentos.as_view()),
    url(r'^documentos/$', Documentos.as_view()),

    url(r'^stories/', Stories.as_view()),
    url(r'^stories/(?P<categoria_id>[\d]+)', Stories.as_view()),

    path('criardocumento/', CriarDocumento.as_view()),
    url(r'^criandodocumento/(?P<doc_id>[\d]+)', CriandoDocumento.as_view()),
    url(r'^deletardocumento/(?P<doc_id>[\d]+)', DeletarDocumento.as_view()),

    url(r'^atualizarlogo/(?P<imagem_w>[\d]+)/(?P<imagem_h>[\d]+)/(?P<imagem_x>-?[\d]+)/(?P<imagem_y>-?[\d]+)', AtualizarLogo.as_view()),


    path('criarstorie/', CriarStorie.as_view()),

    url(r'^criandostorie/(?P<doc_id>[\d]+)', CriandoStorie.as_view()),
    url(r'^deletarstorie/(?P<doc_id>[\d]+)', DeletarStorie.as_view()),



    url(r'^configuracoes/', Configuracoes.as_view()),


    url(r'^trocarplano/$', TrocarPlano.as_view()),
    url(r'^trocarplano/(?P<plano_id>[\d]+)/$', TrocarPlano.as_view()),



    url(r'^busca/', BuscaDocumentos.as_view()),



    url(r'^faqs/', Faqs.as_view()),
    url(r'^faq/(?P<faq_id>[\d]+)/$', Faq.as_view()),


    url(r'^baixarstorie/(?P<s_id>[\d]+)/$', DownloadStorieView.as_view()),
    url(r'^baixardocumento/(?P<d_id>[\d]+)/$', DownloadDocumento.as_view()),



    url(r'^inativarconta/', Inativar.as_view()),




    path('', Index.as_view()),




]

urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urlpatterns += static.static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


handler404 = 'servico.views.error_404_view'