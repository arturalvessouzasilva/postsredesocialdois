from django.db import models
from django.contrib.auth.models import User

from django.conf import settings

from django.utils import timezone


class Plano(models.Model):

    valor = models.DecimalField('valor', default=1.0, decimal_places=2, max_digits=6)

    titulo = models.CharField(max_length=25, default="")

    descricao = models.CharField(max_length=100, default="")

    numero_downloads_direito = models.IntegerField('numero_downloads_direito', default=0)

    def __str__(self):
        return self.titulo



class Perfil(models.Model):

    user = models.OneToOneField(User, related_name='usuario', on_delete=models.CASCADE)


    logo = models.ImageField(upload_to = 'logo', default = settings.LOGO_ROOT+'/None/no-img.jpg', null=True, blank=True)
    fezcrop = models.BooleanField(default=False)

    telefone = models.CharField(max_length=15, default="")

    endereco = models.CharField(max_length=500, default="")

    numero_downloads_direito = models.IntegerField('numero_downloads_direito', default=0)

    plano = models.ForeignKey(Plano, null=True, blank=False, on_delete=models.SET_NULL)

    inativo = models.BooleanField(default=False)

    data_plano = models.DateTimeField(default=timezone.now)


    def __str__(self):
        return self.user.username



class FAQ(models.Model):

    titulo = models.CharField(max_length=50, default="")

    texto = models.CharField(max_length=1000, default="")

    videofile = models.FileField(upload_to='videos/', null=True, blank=True, verbose_name="")



    def __str__(self):
        return str(self.id) + " " + self.titulo



class SolicitacaoPlano(models.Model):

    plano = models.ForeignKey(Plano, null=True, blank=False, on_delete=models.SET_NULL)

    perfil = models.ForeignKey(Perfil, null=False, blank=False, on_delete=models.CASCADE)

    data_criacao = models.DateTimeField(default=timezone.now)


    def __str__(self):
        return str(self.data_criacao)



class TextoPublico(models.Model):

    texto = models.CharField(max_length=500, default="")

    def __str__(self):
        return str(self.texto)

class ImagemPublico(models.Model):

    imagem = models.ImageField(upload_to = 'imagempublica', null=True, blank=True)


    def __str__(self):
        return str(self.imagem)


class VideoPublico(models.Model):

    videofile = models.FileField(upload_to='videosindex/', null=True, blank=True, verbose_name="")


    def __str__(self):
        return str(self.videofile)
