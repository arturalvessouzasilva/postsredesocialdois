from django import forms


class RegistroFORM(forms.Form):

    email = forms.CharField(
        required=True,
        label='email',
    )

    password = forms.CharField(
        required=True,
        label='password',
        widget=forms.PasswordInput(),
    )

    password_confirm = forms.CharField(
        required=True,
        label='password_confirm',
        widget=forms.PasswordInput(),
    )

    telefone = forms.CharField(
        required=True,
        label='telefone',
    )

    endereco = forms.CharField(
        required=True,
        label='endereco',
    )

    plano = forms.IntegerField(
        required=False,
        label='plano',
    )


class UpdateRegistroFORM(forms.Form):
    img = forms.ImageField(
        required=False,
    )



    email = forms.CharField(
        required=False,
        label='email',
    )

    password = forms.CharField(
        required=False,
        label='password',
        widget=forms.PasswordInput(),
    )

    password_confirm = forms.CharField(
        required=False,
        label='password_confirm',
        widget=forms.PasswordInput(),
    )

    telefone = forms.CharField(
        required=False,
        label='telefone',
    )

    endereco = forms.CharField(
        required=False,
        label='endereco',
    )

    nome = forms.CharField(
        required=False,
        label='nome',
    )

    sobrenome = forms.CharField(
        required=False,
        label='sobrenome',
    )





class LoginForm(forms.Form):

    email = forms.CharField(
        required=True,
        label='email',
    )
    password = forms.CharField(
        required=True,
        label='password',
        widget=forms.PasswordInput(),
    )
