from django.contrib import admin


from .models import *

admin.site.register(Plano)
admin.site.register(Perfil)
admin.site.register(FAQ)
admin.site.register(SolicitacaoPlano)
admin.site.register(TextoPublico)
admin.site.register(ImagemPublico)
admin.site.register(VideoPublico)

