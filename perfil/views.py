from django.shortcuts import render

from django.views.generic import View
from django.contrib.auth.models import User

from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect

import random

from django.http import JsonResponse, HttpResponse

from django.core.mail import send_mail

from .forms import *
from .models import *
from servico.models import *

from datetime import datetime, timedelta


import magic


class Index(View):

    def get(self, request):

        categorias = Categoria.objects.all()

        user = request.user

        if not user.is_authenticated:

            textos = TextoPublico.objects.all().order_by('id')
            imagens = ImagemPublico.objects.all().order_by('id')
            video = VideoPublico.objects.all().first()

            return render(
                request,
                'indexpublico.html',
                context={'textos': textos, 'imagens': imagens, 'video': video},
            )

        perfil = Perfil.objects.filter(user=user).first()


        documentos = Documento.objects.all()[:3]
        documentos_retornar = []
        for docu in documentos:
            variacao = VariacaoDocumento.objects.filter(variacao_filho_doc=docu)
            if variacao.count() > 0:
                continue
            imagens = IMG_Doc.objects.filter(documento=docu)
            documentos_retornar.append( (docu, imagens) )

        stories = Storie.objects.all()[:3]
        stories_retornar = []
        for docu in stories:
            variacao = VariacaoStorie.objects.filter(variacao_filho=docu)
            if variacao.count() > 0:
                continue
            imagens = IMG_Stories.objects.filter(storie=docu)
            stories_retornar.append( (docu, imagens) )


        # se a data for pra add downloads
        proxima_renovacao = perfil.data_plano + timedelta(days=30)
        if proxima_renovacao <= timezone.now():
            perfil.numero_downloads_direito = perfil.numero_downloads_direito + perfil.plano.numero_downloads_direito
            perfil.data_plano = perfil.data_plano + timedelta(days=30)
            perfil.save()


        return render(
            request,
            'index.html',
            context={'categoria': categorias, 'documentos': documentos_retornar, 'stories': stories_retornar},
        )


class Login(View):

    def get(self, request):

        user = request.user

        if user.is_authenticated:
            return HttpResponseRedirect("/")

        return render(
            request,
            'login.html',
            context={},
        )

    def post(self, request):

        form = LoginForm(request.POST)

        if form.is_valid():
            userObj = form.cleaned_data
            email = userObj['email']
            password = userObj['password']

            if email == "":
                return render(request, 'login.html', {'error': "Email é obrigatório"})

            if password == "":
                return render(request, 'login.html', {'error': "Senha é obrigatório"})


            perfil = Perfil.objects.filter(user__username= email).first()

            if perfil.inativo:
                return render(request, 'login.html', {'error': "Usuário inativo"})


            user = authenticate(request, username=email, password=password)

            if user is not None:
                login(request, user)
                return HttpResponseRedirect("/")
            else:
                return render(request, 'login.html', {'error': "Usuário não encontrado / Senha errada"})

        else:
            campo_obrigatorio = []
            for k in list(form.errors.keys()):
                campo_obrigatorio.append(k)
            return render(request, 'login.html', {'erro_formulario': campo_obrigatorio})


class Logout(View):

    def get(self, request):
        logout(request)
        return HttpResponseRedirect("/")



class ReseteSenha(View):

    def get(self, request):

        user = request.user

        if not user.is_authenticated:
            return HttpResponseRedirect("/")

        user = request.user

        nova_senha = "".join(random.sample(sequencia_chars_nova_senha, 12))

        user.set_password(nova_senha)
        user.save()

        print("nova senha")
        print(nova_senha)

        print("email")
        print(user.email)



        try:
            send_mail(
                'Sua nova senha',
                'Senha: ' + nova_senha,
                '',
                [user.email],
                fail_silently=False,
            )
        except Exception as e:
            print(e)



        # logout(request)
        return HttpResponseRedirect("/")


class Registro(View):


    def get(self, request):

        user = request.user

        if user.is_authenticated:

            perfil = Perfil.objects.filter(user=user).first()

            if not perfil:
                return HttpResponseRedirect("/registro")

            categorias = Categoria.objects.all()

            form = UpdateRegistroFORM

            return render(
                request,
                'update_registro.html',
                context={'perfil': perfil, 'categoria': categorias, 'form': form},
            )
        return
        planos = Plano.objects.all()

        return render(
            request,
            'registro.html',
            context={'planos': planos},
        )

    def post(self, request):

        form = RegistroFORM(request.POST)

        if form.is_valid():
            userObj = form.cleaned_data
            email = userObj['email']
            password = userObj['password']
            password_confirm = userObj['password_confirm']
            telefone = userObj['telefone']
            endereco = userObj['endereco']
            plano_formulario = userObj['plano']

            if email == "":
                return render(request, 'registro.html', {'error': "Email é obrigatório"})

            if password == "":
                return render(request, 'registro.html', {'error': "Senha é obrigatório"})

            if password != password_confirm:
                return render(request, 'registro.html', {'error': "Senhas devem ser iguais"})

            if plano_formulario == "":
                return render(request, 'registro.html', {'error': "Escolha um Plano"})

            try:
                user = User.objects.create_user(username=email, email=email, password=password)
                user.save()

                plano = Plano.objects.filter(id=plano_formulario).first()
                if not plano:
                    Plano.objects.all().first()

                profile = Perfil(user=user, telefone=telefone, endereco=endereco, plano=plano, numero_downloads_direito=plano.numero_downloads_direito)

                profile.save()

                login(request, user)
            except Exception as e:
                return render(request, 'registro.html', {'error': 'Erro ao salvar usuário, possível email já cadastrado.'})

            categorias = Categoria.objects.all()

            return render(request, 'index.html', {'categorias': categorias})

        else:
            campo_obrigatorio = []
            for k in list(form.errors.keys()):
                campo_obrigatorio.append(k)
            return render(request, 'registro.html', {'erro_formulario': campo_obrigatorio})



class UpdateRegistro(View):

    def post(self, request):
        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")

        #print("AAAAAAA")
        #print(request.POST)
        form = UpdateRegistroFORM(request.POST, request.FILES)
        # form = UpdateRegistro(request.POST, request.FILES)


        if form.is_valid():
            userObj = form.cleaned_data
            img = userObj['img']
            password = userObj['password']
            password_confirm = userObj['password_confirm']
            telefone = userObj['telefone']

            try:
                perfil = Perfil.objects.filter(user=user).first()

                if img != None:

                    perfil.logo = img
                    perfil.fezcrop = False

                if password != None and password !="" and password == password_confirm:
                    perfil.set_password( password )

                if telefone != None and telefone !="":
                    perfil.telefone = telefone




                perfil.save()

            except Exception as e:
                categorias = Categoria.objects.all()

                return render(request, 'update_registro.html', {'categorias':categorias , 'error': 'Erro ao salvar usuário, possível email já cadastrado.'})

            return HttpResponseRedirect("/registro")


        else:
            return HttpResponseRedirect("/registro")




class Faqs(View):


    def get(self, request):

        user = request.user

        if not user.is_authenticated:
            return HttpResponseRedirect("/login")

        categorias = Categoria.objects.all()


        faqs = FAQ.objects.all()

        return render(
            request,
            'faqs.html',
            context={'categoria':categorias, 'faqs': faqs},
        )


class Faq(View):


    def get(self, request, faq_id=1):

        user = request.user

        if not user.is_authenticated:
            return HttpResponseRedirect("/login")

        categorias = Categoria.objects.all()


        faq = FAQ.objects.filter(id=faq_id).first()

        tipo = ""

        if faq.videofile:


            mime = magic.Magic(mime=True)
            tipo = mime.from_file(settings.MEDIA_ROOT + "/" + str(faq.videofile))

            if tipo == "video/mp4":
                tipo = "video"


        return render(
            request,
            'faq.html',
            context={'categoria':categorias, 'faq': faq, 'tipo': tipo},
        )



class Inativar(View):


    def get(self, request, faq_id=1):

        user = request.user

        if not user.is_authenticated:
            return HttpResponseRedirect("/login")

        perfil = Perfil.objects.filter( user=user )

        perfil.inativo = True
        perfil.save()

        logout(request)


        return HttpResponseRedirect("/")

