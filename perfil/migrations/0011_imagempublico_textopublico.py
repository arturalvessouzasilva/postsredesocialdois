# Generated by Django 2.2.4 on 2019-10-04 16:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('perfil', '0010_auto_20190911_1549'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImagemPublico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imagem', models.ImageField(blank=True, null=True, upload_to='imagempublica')),
            ],
        ),
        migrations.CreateModel(
            name='TextoPublico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(default='', max_length=500)),
            ],
        ),
    ]
