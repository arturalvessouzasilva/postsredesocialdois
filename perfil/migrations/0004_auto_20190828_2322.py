# Generated by Django 2.2.4 on 2019-08-28 23:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('perfil', '0003_auto_20190823_0050'),
    ]

    operations = [
        migrations.CreateModel(
            name='FAQ',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(default='', max_length=50)),
                ('texto', models.CharField(default='', max_length=1000)),
                ('videofile', models.FileField(null=True, upload_to='videos/', verbose_name='')),
            ],
        ),
        migrations.CreateModel(
            name='Plano',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('valor', models.DecimalField(decimal_places=2, default=1.0, max_digits=6, verbose_name='valor')),
                ('titulo', models.CharField(default='', max_length=25)),
                ('descricao', models.CharField(default='', max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='perfil',
            name='numero_downloads_direito',
            field=models.IntegerField(default=0, verbose_name='numero_downloads_direito'),
        ),
        migrations.AddField(
            model_name='perfil',
            name='numero_downloads_feito',
            field=models.IntegerField(default=0, verbose_name='numero_downloads_feito'),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='logo',
            field=models.ImageField(blank=True, default='/media/artur/1d54c699-f29b-4f82-a4c5-1a37c11b8400/artur/TRABALHO/posts_redesocial/postredesocial_projeto/media/logo/None/no-img.jpg', null=True, upload_to='logo'),
        ),
        migrations.AddField(
            model_name='perfil',
            name='plano',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='perfil.Plano'),
        ),
    ]
