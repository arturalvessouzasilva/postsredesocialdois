# Generated by Django 2.2.4 on 2019-09-11 15:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('perfil', '0009_perfil_fezcrop'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faq',
            name='videofile',
            field=models.FileField(blank=True, null=True, upload_to='videos/', verbose_name=''),
        ),
    ]
