from django.db import models

# Create your models here.
from perfil.models import *
from django.conf import settings

from django.utils import timezone

# import datetime




class Categoria(models.Model):

    titulo = models.CharField(max_length=25, default="")

    descricao = models.CharField(max_length=100, default="")


    def __str__(self):
        return self.titulo + " " + str(self.id)


class Documento(models.Model):

    titulo = models.CharField(max_length=50, default="")
    descricao = models.CharField(max_length=50, default="")

    logo_x = models.IntegerField(default=0)
    logo_y = models.IntegerField(default=0)
    logo_largura = models.IntegerField(default=0)
    logo_altura = models.IntegerField(default=0)


    data_criacao = models.DateTimeField(default=timezone.now)
    # data_criacao = models.DateTimeField(blank=True, null=True)

    categorias = models.ManyToManyField(Categoria , blank=True)



    maximo_downloads = models.IntegerField(default=100)
    bloqueado = models.BooleanField(default=False)




    def __str__(self):
        return self.titulo + " " + str(self.id)


class VariacaoDocumento(models.Model):
    variacao_pai_doc = models.ForeignKey(Documento, null=False, blank=False, on_delete=models.CASCADE, related_name="variacao_pai")
    variacao_filho_doc = models.ForeignKey(Documento, null=False, blank=False, on_delete=models.CASCADE, related_name="variacao_filho")




class IMG_Doc(models.Model):


    documento = models.ForeignKey(Documento, null=False, blank=False, on_delete=models.CASCADE)

    img = models.ImageField(upload_to = 'imagens', default = settings.IMG_ROOT+'/None/no-img.jpg')


    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)
    largura = models.IntegerField(default=0)
    altura = models.IntegerField(default=0)

    def __str__(self):
        return str(self.img)


class DonoDocumento(models.Model):

    documento = models.ForeignKey(Documento, null=False, blank=False, on_delete=models.CASCADE)

    perfil = models.ForeignKey(Perfil, null=True, blank=True, on_delete=models.SET_NULL)



class Download(models.Model):

    documento = models.ForeignKey(Documento, null=False, blank=False, on_delete=models.CASCADE)

    perfil = models.ForeignKey(Perfil, null=False, blank=False, on_delete=models.CASCADE)

    data = models.DateTimeField(default=timezone.now)


    def __str__(self):
        return self.perfil.user.username



class Storie(models.Model):

    titulo = models.CharField(max_length=50, default="")
    descricao = models.CharField(max_length=50, default="")

    logo_x = models.IntegerField(default=0)
    logo_y = models.IntegerField(default=0)
    logo_largura = models.IntegerField(default=0)
    logo_altura = models.IntegerField(default=0)


    data_criacao = models.DateTimeField(default=timezone.now)
    # data_criacao = models.DateTimeField(blank=True, null=True)

    categorias = models.ManyToManyField(Categoria, blank=True)



    maximo_downloads = models.IntegerField(default=100)
    bloqueado = models.BooleanField(default=False)





    def __str__(self):
        return self.titulo + " " + str(self.id)

class VariacaoStorie(models.Model):
    variacao_pai = models.ForeignKey(Storie, null=False, blank=False, on_delete=models.CASCADE, related_name="variacao_pai")
    variacao_filho = models.ForeignKey(Storie, null=False, blank=False, on_delete=models.CASCADE, related_name="variacao_filho")




class IMG_Stories(models.Model):


    storie = models.ForeignKey(Storie, null=False, blank=False, on_delete=models.CASCADE)

    img = models.ImageField(upload_to = 'imagens', default = settings.IMG_ROOT+'/None/no-img.jpg')


    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)
    largura = models.IntegerField(default=0)
    altura = models.IntegerField(default=0)

    def __str__(self):
        return str(self.img)


class DownloadStorie(models.Model):

    storie = models.ForeignKey(Storie, null=False, blank=False, on_delete=models.CASCADE)

    perfil = models.ForeignKey(Perfil, null=False, blank=False, on_delete=models.CASCADE)

    data = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.perfil.user.username
