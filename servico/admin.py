from django.contrib import admin


from .models import *

admin.site.register(Categoria)
admin.site.register(Documento)
admin.site.register(VariacaoDocumento)
admin.site.register(IMG_Doc)
admin.site.register(DonoDocumento)
admin.site.register(Download)


admin.site.register(Storie)
admin.site.register(VariacaoStorie)
admin.site.register(IMG_Stories)
admin.site.register(DownloadStorie)

