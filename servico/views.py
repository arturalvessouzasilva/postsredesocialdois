from django.shortcuts import render

from django.views.generic import View
from django.contrib.auth.models import User

from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponseBadRequest, JsonResponse, HttpResponse

import random


from datetime import datetime, timedelta
# from django.core.mail import send_mail

from .forms import *
from perfil.models import *
from .models import *

import sys
from PIL import Image

from wsgiref.util import FileWrapper

from itertools import chain

class Documentos(View):

    def get(self, request, categoria_id=-1):


        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        categorias = Categoria.objects.all()


        categoria = Categoria.objects.filter(id=categoria_id).first()

        if not categoria:
            documentos = Documento.objects.filter(bloqueado=False).order_by('-data_criacao')

        else:
            documentos = Documento.objects.filter(categorias=categoria, bloqueado=False)


        documentos_retornar = []

        for docu in documentos:

            pais = VariacaoDocumento.objects.filter(variacao_filho_doc=docu)

            if pais.count() > 0:
                continue


            filhos = VariacaoDocumento.objects.filter(variacao_pai_doc=docu)


            imagens = IMG_Doc.objects.filter(documento=docu)
            documentos_retornar.append( (docu, imagens, filhos) )



        return render(
            request,
            'documentos.html',
            context={ 'documentos': documentos_retornar, 'categoria': categorias },
        )

class Stories(View):

    def get(self, request, categoria_id=-1):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        categorias = Categoria.objects.all()


        categoria = Categoria.objects.filter(id=categoria_id).first()

        if not categoria:
            stories = Storie.objects.filter(bloqueado=False).order_by('-data_criacao')

        else:

            stories = Storie.objects.filter(categorias=categoria, bloqueado=False)


        documentos_retornar = []

        for docu in stories:

            pais = VariacaoStorie.objects.filter(variacao_filho=docu)

            if pais.count() > 0:
                continue


            filhos = VariacaoStorie.objects.filter(variacao_pai=docu)


            imagens = IMG_Stories.objects.filter(storie=docu)
            documentos_retornar.append( (docu, imagens, filhos) )


        return render(
            request,
            'stories.html',
            context={ 'documentos': documentos_retornar, 'categoria': categorias },
        )

class BuscaDocumentos(View):

    def post(self, request):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        categorias = Categoria.objects.all()


        busca = request.POST['busca']

        if busca == None or busca == "":
            busca = "novidade"

        resultado_busca = {
            "layouts": [],
            "stories": [],
            "categorias": [],
        }


        resultado_busca["layouts"].append( Documento.objects.filter(titulo__contains=busca) )
        resultado_busca["layouts"].append( Documento.objects.filter(descricao__contains=busca) )

        resultado_busca["stories"].append( Storie.objects.filter(titulo__contains=busca) )
        resultado_busca["stories"].append( Storie.objects.filter(descricao__contains=busca) )

        resultado_busca["categorias"].append( Categoria.objects.filter(titulo__contains=busca) )
        resultado_busca["categorias"].append( Categoria.objects.filter(descricao__contains=busca) )

        if not resultado_busca["layouts"] and not resultado_busca["stories"] and not resultado_busca["categorias"]:
            return render(
                request,
                'buscadocumentos.html',
                context={ 'erro': 'Nada Encontrado', 'categoria': categorias },
            )


        documentos_retornar = []
        for documento in resultado_busca["layouts"]:
            for docu in documento:

                variacao = VariacaoDocumento.objects.filter(variacao_filho_doc=docu)
                if variacao.count() > 0:
                    continue

                cont = False
                for dcs in documentos_retornar:
                    if dcs[0] == docu:
                        cont = True
                if cont:
                    continue

                imagens = IMG_Doc.objects.filter(documento=docu)
                documentos_retornar.append( (docu, imagens) )
        resultado_busca["layouts"] = documentos_retornar

        documentos_retornar = []
        for documento in resultado_busca["stories"]:
            for docu in documento:
                variacao = VariacaoStorie.objects.filter(variacao_filho=docu)
                if variacao.count() > 0:
                    continue

                cont = False
                for dcs in documentos_retornar:
                    if dcs[0] == docu:
                        cont = True
                if cont:
                    continue

                imagens = IMG_Stories.objects.filter(storie=docu)
                documentos_retornar.append( (docu, imagens) )
        resultado_busca["stories"] = documentos_retornar


        documentos_retornar = []
        for documento in resultado_busca["categorias"]:
            for docu in documento:
                documentos_retornar.append( docu )
        resultado_busca["categorias"] = documentos_retornar



        return render(
            request,
            'buscadocumentos.html',
            context={ 'resultado': resultado_busca, 'categoria': categorias },
        )




class CriarDocumento(View):

    def get(self, request):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()



        doc = Documento(titulo="Documento Novo", descricao="Documento", logo_largura=310, logo_altura=180)
        doc.save()


        return HttpResponseRedirect("/criandodocumento/"+str(doc.id))




class CriandoDocumento(View):

    def get(self, request, doc_id):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        categorias = Categoria.objects.all()

        documento = Documento.objects.filter(id=doc_id).first()


        if not documento:

            return render(
            request,
            'criando_doc.html',
            context={ 'erro': "Documento nao encontrado", 'categoria': categorias },
            )

        imagens = IMG_Doc.objects.filter(documento=documento)

        for ii in imagens:
            if ii.img.width != 1080 or ii.img.height != 1080:
                images = map(Image.open, [settings.MEDIA_ROOT+'/'+str(ii.img)])
                images = list(images)
                im = images[0]
                im.convert('RGBA')
                im = im.resize(( 1080, 1080 ))
                im.save( settings.MEDIA_ROOT+'/' + str(ii.img) )

        maior_larg = 1080/3
        maior_alt = 1080/3

        if documento.logo_largura:
            logo_max_w = documento.logo_largura/3
        else:
            logo_max_w = 310/3

        if documento.logo_altura:
            logo_max_h = documento.logo_altura/3
        else:
            logo_max_h = 180/3

        logo_aux = None

        try:
            logow = perfil.logo.width
            logoh = perfil.logo.height
        except Exception as e:
            logo_aux =  '/logo/None/no-img.jpg'
            logow = 310/3
            logoh = 180/3

        try:
            if logo_max_h < logoh:
                rate_h = logoh / logo_max_h
                logoh = logoh / rate_h
                logow = logow / rate_h


            elif logo_max_w < logow:
                rate_w = logow / logo_max_w
                logow = logow / rate_w
                logoh = logoh / rate_w

        except Exception as e:
            pass


        logoxzao = (documento.logo_x / 3) + ((logo_max_w - logow) / 2)
        logoyzao = (documento.logo_y/ 3) + ((logo_max_h - logoh) / 2)



        variacoes = VariacaoDocumento.objects.filter(variacao_pai_doc=documento)
        resultado_variacoes = []
        for variacao in variacoes:
            imgs = IMG_Doc.objects.filter( documento=variacao.variacao_filho_doc ).first()
            resultado_variacoes.append( (variacao.variacao_filho_doc, imgs) )

        meupai = None
        try:
            variacoesfilha = VariacaoDocumento.objects.filter(variacao_filho_doc=documento).first()
            meupai = variacoesfilha.variacao_pai_doc
        except Exception as e:
            pass


        downloads_feitos_doc = Download.objects.filter(documento=documento).count()

        print(logow)
        print(logoh)

        return render(
            request,
            'criando_doc.html',
            context={ 'categoria':categorias, 'documento': documento, 'imgs': imagens, 'perfil': perfil, 'canvas_larg': maior_larg, 'canvas_alt': maior_alt,
            'logow': logow, 'logoh': logoh, 'logox': logoxzao, 'logoy': logoyzao,
            'variacoes': resultado_variacoes, 'logoaux': logo_aux,
            'meupai': meupai, 'downloads_feitos_doc': downloads_feitos_doc
             },
        )


    def post(self, request, doc_id):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")


        # so admin pode editar
        if not request.user.is_superuser:
            result = {'erro': "Voce nao pode fazer isso"}
            return JsonResponse(result)


        perfil = Perfil.objects.filter(user=user).first()

        documento = Documento.objects.filter(id=doc_id).first()

        if not documento:


            categorias = Categoria.objects.all()

            return render(
                request,
                'criando_doc.html',
                context={ 'erro': "Documento nao encontrado", 'categorias': categorias },
            )


        form = IMGForm(request.POST, request.FILES)
        if form.is_valid():

            logo_x = form.cleaned_data["logo_x"]
            logo_y = form.cleaned_data["logo_y"]
            logo_largura = form.cleaned_data["logo_largura"]
            logo_altura = form.cleaned_data["logo_altura"]
            titulo = form.cleaned_data["titulo"]
            descricao = form.cleaned_data["descricao"]
            image = form.cleaned_data['img']
            variacoes = form.cleaned_data['variacoes']



            if variacoes != "" and variacoes != None:
                varias = VariacaoDocumento.objects.filter(variacao_pai_doc=documento)

                if varias.count() < int(variacoes):
                    for x in range(0, (int(variacoes)-varias.count()) ):
                        doc = Documento(titulo="Documento Novo", descricao="Documento", logo_largura=310, logo_altura=180)
                        doc.save()
                        vari = VariacaoDocumento( variacao_pai_doc=documento, variacao_filho_doc=doc )
                        vari.save()


            if titulo != "" and titulo != None:
                documento.titulo = titulo

            if descricao != "" and descricao != None:
                documento.descricao = descricao


            if logo_x != "" and logo_x != None:
                documento.logo_x = logo_x

            if logo_y != "" and logo_y != None:
                documento.logo_y = logo_y



            if logo_largura != "" and logo_largura != None:
                documento.logo_largura = logo_largura

            if logo_altura != "" and logo_altura != None:
                documento.logo_altura = logo_altura

            documento.save()




            try:
                if image != None and image != "":
                    imgs_apagar = IMG_Doc.objects.filter(documento=documento)
                    for img_apagar in imgs_apagar:
                        img_apagar.delete()

            except Exception as e:
                pass

            try:
                if image != None and image != "":
                    # image = image.resize(( 1080, 1080 ))
                    imagem = IMG_Doc(documento=documento, img=image)
                    imagem.save()
                    imagem.largura = 1080
                    imagem.altura = 1080
                    imagem.save()

            except Exception as e:
                pass




        else:
            return HttpResponseBadRequest()

        return HttpResponseRedirect(self.request.path_info)


class AtualizarImagem(View):

    def get(self, request, imagem_id, imagem_x, imagem_y):


        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        # so admin pode editar
        if not request.user.is_superuser:
            result = {'erro': "Voce nao pode fazer isso"}
            return JsonResponse(result)

        imagem = IMG_Doc.objects.filter(id=imagem_id).first()

        if not imagem:
            result = {'erro': 'Imagem nao encontrada'}
            return JsonResponse(result)

        imagem.x= imagem_x
        imagem.y= imagem_y

        imagem.save()

        result = {'success': True}
        return JsonResponse(result)


class AtualizarLogo(View):

    def get(self, request, imagem_w, imagem_h, imagem_x, imagem_y):

        imagem_w = int( imagem_w )
        imagem_h = int( imagem_h )
        imagem_x = int( imagem_x )
        imagem_y = int( imagem_y )

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()


        imagem = perfil.logo

        if not imagem:
            result = {'erro': 'Logo nao encontrada'}
            return JsonResponse(result)

        # ------------------------

        im = None
        tamanho_original_logo_w = 0
        tamanho_original_logo_h = 0

        x_corte = None
        y_corte = None
        w_corte = None
        h_corte = None
        x_colar = None
        y_colar = None

        try:
            images = map(Image.open, [settings.MEDIA_ROOT+'/'+str(imagem)])
            images = list(images)

            im = images[0]

            tamanho_original_logo_w = im.width
            tamanho_original_logo_h = im.height
        except Exception as e:
            return HttpResponseRedirect("/registro")


        if imagem_x > tamanho_original_logo_w:
            return HttpResponseRedirect("/registro")

        if imagem_y > tamanho_original_logo_h:
            return HttpResponseRedirect("/registro")

        if imagem_x < 0 and (imagem_w + imagem_x) < 1:
            return HttpResponseRedirect("/registro")
        elif imagem_x < 0:
            w_corte = imagem_w + imagem_x
            x_corte = 0
            x_colar = abs(imagem_x)


        if imagem_y < 0 and (imagem_y + imagem_y) < 1:
            return HttpResponseRedirect("/registro")

        elif imagem_y < 0:
            h_corte = imagem_h + imagem_y
            y_corte = 0
            y_colar = abs( imagem_y )




        if imagem_x >= 0 and (imagem_x + imagem_w) > tamanho_original_logo_w :
            w_corte = tamanho_original_logo_w - imagem_x
            x_corte = imagem_x
            x_colar = 0


        elif imagem_x >= 0:
            w_corte = imagem_w + imagem_x
            x_corte = imagem_x
            x_colar = 0



        if imagem_y >= 0 and (imagem_y + imagem_h) > tamanho_original_logo_h :
            h_corte = tamanho_original_logo_h - imagem_y
            y_corte = imagem_y
            y_colar = 0


        elif imagem_y >= 0:
            h_corte = imagem_h + imagem_y
            y_corte = imagem_y
            y_colar = 0




        # return HttpResponseRedirect("/registro")


        try:
            im = im.crop(( x_corte , y_corte , w_corte, h_corte))

        except Exception as e:
            raise


        try:
            background = Image.new('RGBA', (imagem_w, imagem_h), (0, 0, 0, 0))
            background.paste(im, (x_colar, y_colar), im)
            background.save(settings.MEDIA_ROOT+'/'+str(imagem))

        except Exception as e:
            background = Image.new('RGB', (imagem_w, imagem_h), (255, 255, 255))
            background.paste(im, (x_colar, y_colar))

            background.save(settings.MEDIA_ROOT+'/'+str(imagem))

        # perfil.fezcrop = True;
        # perfil.save()


        # ------------------------


        return HttpResponseRedirect("/registro")





class CriarStorie(View):

    def get(self, request):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()


        doc = Storie(titulo="Storie Novo", descricao="Storie", logo_largura=310, logo_altura=180)
        doc.save()

        return HttpResponseRedirect("/criandostorie/"+str(doc.id))



class CriandoStorie(View):


    def get(self, request, doc_id):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()


        categorias = Categoria.objects.all()

        documento = Storie.objects.filter(id=doc_id).first()

        if not documento:

            return render(
            request,
            'criando_storie.html',
            context={ 'erro': "Documento nao encontrado", 'categoria': categorias },
            )

        imagens = IMG_Stories.objects.filter(storie=documento)
        for ii in imagens:
            if ii.img.width != 1080 or ii.img.height != 1920 :
                images = map(Image.open, [settings.MEDIA_ROOT+'/'+str(ii.img)])
                images = list(images)
                im = images[0]
                im.convert('RGBA')
                im = im.resize(( 1080, 1920 ))
                im.save( settings.MEDIA_ROOT+'/' + str(ii.img) )




        canvas_larg = 1080/4
        canvas_alt = 1920/4

        if documento.logo_largura:
            logo_max_w = documento.logo_largura/4
        else:
            logo_max_w = 310/4

        if documento.logo_altura:
            logo_max_h = documento.logo_altura/4
        else:
            logo_max_h = 180/4


        logo_aux = None

        try:
            logow = perfil.logo.width
            logoh = perfil.logo.height
        except Exception as e:
            logo_aux =  '/logo/None/no-img.jpg'

            logow = 310
            logoh = 180



        try:
            if logo_max_h < logoh:
                rate_h = logoh / logo_max_h
                logoh = logoh / rate_h
                logow = logow / rate_h


            elif logo_max_w < logow:
                rate_w = logow / logo_max_w
                logow = logow / rate_w
                logoh = logoh / rate_w
        except Exception as e:
            pass




        logoxzao = (documento.logo_x / 4) + ((logo_max_w - logow) / 2)
        logoyzao = (documento.logo_y / 4) + ((logo_max_h - logoh) / 2)


        variacoes = VariacaoStorie.objects.filter(variacao_pai=documento)
        resultado_variacoes = []
        for variacao in variacoes:
            imgs = IMG_Stories.objects.filter( storie=variacao.variacao_filho ).first()
            resultado_variacoes.append( (variacao.variacao_filho, imgs) )

        meupai = None
        try:
            variacoesfilha = VariacaoStorie.objects.filter(variacao_filho=documento).first()
            meupai = variacoesfilha.variacao_pai
        except Exception as e:
            pass


        return render(
            request,
            'criando_storie.html',
            context={ 'categoria':categorias, 'documento': documento, 'imgs': imagens,
             'perfil': perfil, 'canvas_larg': canvas_larg, 'canvas_alt': canvas_alt,
             'logow': logow, 'logoh': logoh, 'logox': logoxzao, 'logoy': logoyzao,
             'variacoes': resultado_variacoes, 'logoaux': logo_aux,
             'meupai': meupai

              },
        )



    def post(self, request, doc_id):


        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")


        # so admin pode editar
        if not request.user.is_superuser:
            result = {'erro': "Voce nao pode fazer isso"}
            return JsonResponse(result)


        perfil = Perfil.objects.filter(user=user).first()

        documento = Storie.objects.filter(id=doc_id).first()

        if not documento:


            categorias = Categoria.objects.all()


            return render(
                request,
                'criando_storie.html',
                context={ 'erro': "Documento nao encontrado", 'categorias': categorias },
            )



        form = IMGForm(request.POST, request.FILES)
        if form.is_valid():

            logo_x = form.cleaned_data["logo_x"]
            logo_y = form.cleaned_data["logo_y"]
            logo_largura = form.cleaned_data["logo_largura"]
            logo_altura = form.cleaned_data["logo_altura"]
            titulo = form.cleaned_data["titulo"]
            descricao = form.cleaned_data["descricao"]
            imag = form.cleaned_data['img']
            variacoes = form.cleaned_data['variacoes']



            if variacoes != "" and variacoes != None:
                varias = VariacaoStorie.objects.filter(variacao_pai=documento)


                if varias.count() < int(variacoes):
                    for x in range(0, (int(variacoes)-varias.count()) ):
                        doc = Storie(titulo="Documento Novo", descricao="Documento", logo_largura=310, logo_altura=180)
                        doc.save()
                        vari = VariacaoStorie( variacao_pai=documento, variacao_filho=doc )
                        vari.save()

            if titulo != "" and titulo != None:
                documento.titulo = titulo

            if descricao != "" and descricao != None:
                documento.descricao = descricao


            if logo_x != "" and logo_x != None:
                documento.logo_x = logo_x

            if logo_y != "" and logo_y != None:
                documento.logo_y = logo_y



            if logo_largura != "" and logo_largura != None:
                documento.logo_largura = logo_largura

            if logo_altura != "" and logo_altura != None:
                documento.logo_altura = logo_altura

            documento.save()

            try:
                if imag != None and imag != "":
                    imgs_apagar = IMG_Stories.objects.filter(storie=documento)
                    for img_apagar in imgs_apagar:
                        img_apagar.delete()

            except Exception as e:
                pass

            try:
                if imag != None and imag != "":
                    imagem = IMG_Stories(storie=documento, img=imag)
                    imagem.save()
                    imagem.largura = 1080
                    imagem.altura = 1920
                    imagem.save()
            except Exception as e:
                pass


        else:
            return HttpResponseBadRequest()

        return HttpResponseRedirect(self.request.path_info)
        # result = {'success': True}
        # return JsonResponse(result)



class Configuracoes(View):

    def get(self, request):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        categorias = Categoria.objects.all()

        posts_baixados = Download.objects.filter(perfil=perfil)
        stories_baixados = DownloadStorie.objects.filter(perfil=perfil)
        # enviar = posts_baixados + stories_baixados

        posts_baixados = posts_baixados.count() +  stories_baixados.count()


        proxima_renovacao = perfil.data_plano + timedelta(days=30)



        return render(
            request,
            'configs.html',
            context={ 'categoria': categorias, 'perfil': perfil,
                'posts_baixados': posts_baixados,
                'proxima_renovacao': proxima_renovacao
             },
        )



    def post(self, request):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        categorias = Categoria.objects.all()


        return render(
            request,
            'configs.html',
            context={ 'categoria': categorias
             },
        )




class TrocarPlano(View):

    def get(self, request, plano_id=1):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        categorias = Categoria.objects.all()
        posts_baixados = Download.objects.filter(perfil=perfil)
        stories_baixados = DownloadStorie.objects.filter(perfil=perfil)


        planos = Plano.objects.all()


        if plano_id != None and plano_id != "":
            palno_escolhido = Plano.objects.filter(id=plano_id).first()
            solicitacao = SolicitacaoPlano(plano = palno_escolhido, perfil=perfil)

            solicitacao.save()


        return render(
            request,
            'trocar_plano.html',
            context={ 'categoria': categorias, 'planos': planos, 'perfil': perfil,
                'posts_baixados': posts_baixados, 'stories_baixados': stories_baixados,
             },

        )



class DownloadStorieView(View):

    def get(self, request, s_id=1):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        downloads_numero = DownloadStorie.objects.filter(perfil=perfil).count() + Download.objects.filter(perfil=perfil).count()

        if downloads_numero >= perfil.numero_downloads_direito:
            return HttpResponseRedirect("/")

        categorias = Categoria.objects.all()
        documento = Storie.objects.filter(id=s_id).first()


        storie = Storie.objects.filter(id=s_id).first()

        img_fundo = IMG_Stories.objects.filter(storie=storie).first().img

        if perfil.logo:
            images = map(Image.open, [settings.MEDIA_ROOT+'/'+str(img_fundo), settings.MEDIA_ROOT+'/'+str(perfil.logo)])
        else:
            images = map(Image.open, [settings.MEDIA_ROOT+'/'+str(img_fundo), settings.MEDIA_ROOT+'/logo/None/no-img.jpg'])


        images = list(images)

        total_width = images[0].size[0]
        max_height = images[0].size[1]
        new_im = Image.new('RGBA', (total_width, max_height))

        x_offset = 0
        new_im.paste(images[0], (x_offset,0))


        if documento.logo_largura:
            logo_max_w = documento.logo_largura
        else:
            logo_max_w = 310

        if documento.logo_altura:
            logo_max_h = documento.logo_altura
        else:
            logo_max_h = 180

        try:
            logow = perfil.logo.width
            logoh = perfil.logo.height


        except Exception as e:
            logow = 310
            logoh = 180


        logx = documento.logo_x
        logy = documento.logo_y
        if 1080 > total_width:
            logx = logx / ( 1080 / total_width )
            logow = logow / ( 1080 / total_width )
            logo_max_w = logo_max_w / ( 1080 / total_width )

        if 1080 > max_height:
            logy = logy / ( 1080 / max_height )
            logoh = logoh / ( 1080 / max_height )
            logo_max_h = logo_max_h / ( 1080 / max_height )




        if logo_max_h > logoh:
            rate_h = logoh / logo_max_h
            logoh = logoh / rate_h
            logow = logow / rate_h
        elif logo_max_w > logow:
            rate_h = logoh / logo_max_w
            logoh = logoh / rate_h
            logow = logow / rate_h

        if logo_max_h < logoh:
            rate_h = logoh / logo_max_h
            logoh = logoh / rate_h
            logow = logow / rate_h
        elif logo_max_w < logow:
            rate_w = logow / logo_max_w
            logow = logow / rate_w
            logoh = logoh / rate_w


        if logow < logo_max_w:
            logoxzao = logx + ((logo_max_w - logow) / 2)
        else:
            logoxzao = logx


        if logoh < logo_max_h:
            logoyzao = logy + ((logo_max_h - logoh) / 2)
        else:
            logoyzao = logy


        im = images[1]
        im.convert('RGBA')
        im = im.resize(( int(logow), int(logoh) ))
        x_offset = int(logoxzao)
        y_offset = int(logoyzao)

        try:
            new_im.paste(im, (x_offset,y_offset), im)
        except Exception as e:
            new_im.paste(im, (x_offset,y_offset))




        sequencia_chars_nova_senha = "aebgfdchijklmnopqrstuvwzyx"
        nome_img = "".join(random.sample(sequencia_chars_nova_senha, 12))
        nome_img = settings.MEDIA_ROOT+'/downloads/' + nome_img +'.png'

        new_im.save(nome_img)


        d = DownloadStorie(perfil= perfil, storie=storie)
        d.save()

        if DownloadStorie.objects.filter(storie=storie).count() >= storie.maximo_downloads:
            storie.bloqueado = True
            storie.save()

        print(x_offset)
        print(y_offset)

        print(logow)
        print(logoh)


        wrapper = FileWrapper(open(nome_img, 'rb'))
        response = HttpResponse(wrapper, content_type="image/png")
        response['Content-Disposition'] = "attachment; filename="+nome_img[-16:]
        return response





class DownloadDocumento(View):

    def get(self, request, d_id=1):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()

        downloads_numero = DownloadStorie.objects.filter(perfil=perfil).count() + Download.objects.filter(perfil=perfil).count()

        if downloads_numero >= perfil.numero_downloads_direito:
            return HttpResponseRedirect("/")

        categorias = Categoria.objects.all()


        documento = Documento.objects.filter(id=d_id).first()

        img_fundo = IMG_Doc.objects.filter(documento=documento).first().img

        if perfil.logo:
            images = map(Image.open, [settings.MEDIA_ROOT+'/'+str(img_fundo), settings.MEDIA_ROOT+'/'+str(perfil.logo)])
        else:
            images = map(Image.open, [settings.MEDIA_ROOT+'/'+str(img_fundo), settings.MEDIA_ROOT+'/logo/None/no-img.jpg'])


        images = list(images)

        total_width = images[0].size[0]
        max_height = images[0].size[1]
        new_im = Image.new('RGBA', (total_width, max_height))

        x_offset = 0
        new_im.paste(images[0], (x_offset,0))




        if documento.logo_largura:
            logo_max_w = documento.logo_largura
        else:
            logo_max_w = 310

        if documento.logo_altura:
            logo_max_h = documento.logo_altura
        else:
            logo_max_h = 180

        try:
            logow = perfil.logo.width
            logoh = perfil.logo.height


        except Exception as e:
            logow = 310
            logoh = 180


        logx = documento.logo_x
        logy = documento.logo_y
        if 1080 > total_width:
            logx = logx / ( 1080 / total_width )
            logow = logow / ( 1080 / total_width )
            logo_max_w = logo_max_w / ( 1080 / total_width )

        if 1080 > max_height:
            logy = logy / ( 1080 / max_height )
            logoh = logoh / ( 1080 / max_height )
            logo_max_h = logo_max_h / ( 1080 / max_height )




        if logo_max_h > logoh:
            rate_h = logoh / logo_max_h
            logoh = logoh / rate_h
            logow = logow / rate_h
        elif logo_max_w > logow:
            rate_h = logoh / logo_max_w
            logoh = logoh / rate_h
            logow = logow / rate_h

        if logo_max_h < logoh:
            rate_h = logoh / logo_max_h
            logoh = logoh / rate_h
            logow = logow / rate_h
        elif logo_max_w < logow:
            rate_w = logow / logo_max_w
            logow = logow / rate_w
            logoh = logoh / rate_w


        if logow < logo_max_w:
            logoxzao = logx + ((logo_max_w - logow) / 2)
        else:
            logoxzao = logx


        if logoh < logo_max_h:
            logoyzao = logy + ((logo_max_h - logoh) / 2)
        else:
            logoyzao = logy


        im = images[1]
        im.convert('RGBA')
        im = im.resize(( int(logow), int(logoh) ))
        x_offset = int(logoxzao)
        y_offset = int(logoyzao)

        try:
            new_im.paste(im, (x_offset,y_offset), im)
        except Exception as e:
            new_im.paste(im, (x_offset,y_offset))


        sequencia_chars_nova_senha = "aebgfdchijklmnopqrstuvwzyx"
        nome_img = "".join(random.sample(sequencia_chars_nova_senha, 12))
        nome_img = settings.MEDIA_ROOT+'/downloads/' + nome_img +'.png'

        new_im.save(nome_img)

        d = Download(perfil= perfil, documento=documento)
        d.save()

        if Download.objects.filter(documento=documento).count() >= documento.maximo_downloads:
            documento.bloqueado = True
            documento.save()

        wrapper = FileWrapper(open(nome_img, 'rb'))
        response = HttpResponse(wrapper, content_type="image/png")
        response['Content-Disposition'] = "attachment; filename= "+nome_img[-16:]
        return response




def error_404_view(request, exception):
    data = {"name": "ThePythonDjango.com"}
    return render(request,'error_404.html', data)






class DeletarDocumento(View):


    def get(self, request, doc_id):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()


        categorias = Categoria.objects.all()

        documento = Documento.objects.filter(id=doc_id).first()

        if not documento:

            return render(
            request,
            'criando_storie.html',
            context={ 'erro': "Documento nao encontrado", 'categoria': categorias },
            )


        # so admin pode editar
        if not request.user.is_superuser:
            result = {'erro': "Voce nao pode fazer isso"}
            return JsonResponse(result)

        imgs = IMG_Doc.objects.filter(documento=documento)

        for img in imgs:
            img.delete()


        variacoes = VariacaoDocumento.objects.filter(variacao_pai_doc=documento)
        for vari in variacoes:
            vari.delete()

        documento.delete()


        return HttpResponseRedirect("/documentos")



class DeletarStorie(View):


    def get(self, request, doc_id):

        user = request.user
        if not user.is_authenticated:
            return HttpResponseRedirect("/login")
        perfil = Perfil.objects.filter(user=user).first()


        categorias = Categoria.objects.all()

        documento = Storie.objects.filter(id=doc_id).first()

        if not documento:

            return render(
            request,
            'criando_storie.html',
            context={ 'erro': "Documento nao encontrado", 'categoria': categorias },
            )


        # so admin pode editar
        if not request.user.is_superuser:
            result = {'erro': "Voce nao pode fazer isso"}
            return JsonResponse(result)

        imgs = IMG_Stories.objects.filter(storie=documento)

        for img in imgs:
            img.delete()


        variacoes = VariacaoStorie.objects.filter(variacao_pai=documento)
        for vari in variacoes:
            vari.delete()

        documento.delete()


        return HttpResponseRedirect("/stories")
