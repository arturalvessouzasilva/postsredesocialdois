# Generated by Django 2.2.4 on 2019-09-06 03:46

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('servico', '0010_auto_20190906_0213'),
    ]

    operations = [
        migrations.AddField(
            model_name='downloadstorie',
            name='data',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
