# Generated by Django 2.2.4 on 2019-09-09 19:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servico', '0012_variacaodocumento_variacaostorie'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documento',
            name='categorias',
            field=models.ManyToManyField(blank=True, to='servico.Categoria'),
        ),
        migrations.AlterField(
            model_name='storie',
            name='categorias',
            field=models.ManyToManyField(blank=True, to='servico.Categoria'),
        ),
    ]
