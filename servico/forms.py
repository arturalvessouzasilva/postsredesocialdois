from django import forms
from .models import IMG_Doc

class UploadIMG(forms.Form):

    email = forms.CharField(
        required=True,
        label='titulo',
    )

# class IMGForm(forms.ModelForm):
#     class Meta:
#         model = IMG_Doc
#         fields = ('img',)

class IMGForm(forms.Form):
    img = forms.ImageField(
        required=False,
    )

    logo_x = forms.IntegerField(
        required=False,
        label='logo_x',
    )

    logo_y = forms.IntegerField(
        required=False,
        label='logo_y',
    )

    logo_largura = forms.IntegerField(
        required=False,
        label='logo_largura',
    )

    logo_altura = forms.IntegerField(
        required=False,
        label='logo_altura',
    )

    titulo = forms.CharField(
        required=False,
        label='titulo',
    )

    descricao = forms.CharField(
        required=False,
        label='descricao',
    )

    variacoes = forms.IntegerField(
        required=False,
        label='variacoes',
    )